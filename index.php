<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="library/css/bootstrap/bootstrap.css"/>
    <link rel="stylesheet" href="library/css/style.css"/>
    <link rel="stylesheet" href="library/css/pages/homepage.css"/>
    <title>Teste, tá?</title>
  </head>
  <body>
    <section id="section-main">
      <div class="extra"></div>
      <h1 class="text-center">Site Claybom</h1><a href="" class="btn btn-success text-center">Comprar agora</a>
    </section>
    <section id="section-popular" class="mt-5">
      <div class="wrap">
        <div class="col-xs-12 col-sm-6 col-md-3">
          <div class="card"><img src="..." alt="Car Image Cap" class="card-img-top"/>
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p><a href="#" class="btn btn-primary">Comprar</a>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
          <div class="card"><img src="..." alt="Car Image Cap" class="card-img-top"/>
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p><a href="#" class="btn btn-primary">Comprar</a>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
          <div class="card"><img src="..." alt="Car Image Cap" class="card-img-top"/>
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p><a href="#" class="btn btn-primary">Comprar</a>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
          <div class="card"><img src="..." alt="Car Image Cap" class="card-img-top"/>
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p><a href="#" class="btn btn-primary">Comprar</a>
            </div>
          </div>
        </div>
      </div>
    </section>
  </body>
</html>