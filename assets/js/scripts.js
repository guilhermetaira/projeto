(function($) 
{
	$(document).ready(function() 
	{	
		
		document.body.addEventListener('mousedown', function() 
		{
			document.body.classList.add('using-mouse');
		});

		document.body.addEventListener('keydown', function(event) 
		{
			if (event.keyCode === 9) 
			{
				document.body.classList.remove('using-mouse');
		  	}
		});

		$('.btn-play').on('click', function()
		{
		    $('video').each(function(index) 
		    {
				this.pause();
			});

			$(this).fadeOut();
			$($($(this).parent()).find('video')).show().get(0).play();
		});

		/****************************
		 * CONTACT FORM
		*****************************/
		if($("#contact-form").length > 0)
		{
			contactForm = document.getElementById("contact-form");
			contactEmail = document.getElementById("contact-form-email");
			contactName = document.getElementById("contact-form-name");
			contactPhone = document.getElementById("contact-form-phone");
			contactPhoneDDD = document.getElementById("contact-form-phone-ddd");
			contactSchool = document.getElementById("contact-form-school");
			contactCity = document.getElementById("contact-form-city");
			contactState = document.getElementById("contact-form-state");
			contactRole = document.getElementById("contact-form-role");
			contactSegment = document.getElementById("contact-form-segment");
			contactEducationSystem = document.getElementById("contact-form-education-system");
			contactDoubts = document.getElementById("contact-form-doubts");

			contactEmail.addEventListener('keydown', function(e) { contactEmail.classList.remove('error'); });
			contactName.addEventListener('keydown', function(e) { contactName.classList.remove('error'); });
			contactPhone.addEventListener('keydown', function(e) { contactPhone.classList.remove('error'); });
			contactPhoneDDD.addEventListener('keydown', function(e) { contactPhoneDDD.classList.remove('error'); });
			contactSchool.addEventListener('keydown', function(e) { contactSchool.classList.remove('error'); });
			contactCity.addEventListener('keydown', function(e) { contactCity.classList.remove('error'); });
			contactState.addEventListener('onchange', function(e) { contactState.classList.remove('error'); });
			contactRole.addEventListener('keydown', function(e) { contactRole.classList.remove('error'); });
			contactSegment.addEventListener('onchange', function(e) { contactSegment.classList.remove('error'); });
			contactEducationSystem.addEventListener('keydown', function(e) { contactEducationSystem.classList.remove('error'); });
			contactDoubts.addEventListener('keydown', function(e) { contactDoubts.classList.remove('error'); });

			contactForm.addEventListener("submit", function(e) 
			{
				let errors = 0;

				contactEmail.classList.remove('error');
				contactName.classList.remove('error');
				contactPhone.classList.remove('error');
				contactPhoneDDD.classList.remove('error');
				contactSchool.classList.remove('error');
				contactCity.classList.remove('error');
				contactState.classList.remove('error');
				contactRole.classList.remove('error');
				contactSegment.classList.remove('error');
				contactEducationSystem.classList.remove('error');
				contactDoubts.classList.remove('error');

				if (contactName.value == "" || contactName.value.length < 3)
				{
					contactName.classList.add('error');
					errors++;
					
				}

				if (contactEmail.value == "" || contactEmail.value.length < 3)
				{
					contactEmail.classList.add('error');
					errors++;
				}

				if (contactPhoneDDD.value == "" || contactPhoneDDD.value.length < 2)
				{
					contactPhoneDDD.classList.add('error');
					errors++;
				}

				if (contactPhone.value == "" || contactPhone.value.length < 8)
				{
					contactPhone.classList.add('error');
					errors++;
				}

				if (contactSchool.value == "" || contactSchool.value.length < 3)
				{
					contactSchool.classList.add('error');
					errors++;
				}

				if (contactCity.value == "" || contactCity.value.length < 3)
				{
					contactCity.classList.add('error');
					errors++;
				}

				if (contactState.value == "" || contactState.value.length < 3)
				{
					contactState.classList.add('error');
					errors++;
				}

				if (contactRole.value == "" || contactRole.value.length < 3)
				{
					contactRole.classList.add('error');
					errors++;
				}

				if (contactSegment.value == "" || contactSegment.value.length < 3)
				{
					contactSegment.classList.add('error');
					errors++;
				}

				if (contactEducationSystem.value == "" || contactEducationSystem.value.length < 3)
				{
					contactEducationSystem.classList.add('error');
					errors++;
				}

				if (errors == 0)
				{
					jQuery('#contact-form input[type="submit"]').attr('disabled', 'disabled');

					//Create FormData and submit
					var formData = new FormData();
					formData.append('name', contactName.value);
					formData.append('requester', contactEmail.value);
					formData.append('cargo', contactRole.value);
					formData.append('ddd', contactPhoneDDD.value);
					formData.append('telefone', contactPhone.value);
					formData.append('escola', contactSchool.value);
					formData.append('segmento', contactSegment.value);
					formData.append('cidade', contactCity.value);
					formData.append('estado', contactState.value);
					formData.append('sistemadeensino', contactEducationSystem.value);
					formData.append('description', contactDoubts.value);
					ajaxified_contact(formData, 'sendcontact_callback');

					// $('#contact-form-loading').show();
				}
				else

				{
					jQuery('#contact-form-message').text('Por favor, é necessário preencher os campos sinalizados acima.');
					jQuery('#contact-form-message').show();
				}

				e.preventDefault();
				return false;
			});
		}

		/******************************
		HEADER
		*******************************/ 
		$('.hamburger').on('click', function()
		{
			$(this).toggleClass('is-active');
			$('#mobile-menu').toggleClass('open');
		});

		$(window).on("load scroll", function() 
		{
			var parallaxElement = $(".parallax_scroll"),
			parallaxQuantity = parallaxElement.length;

			window.requestAnimationFrame(function() 
			{
				for (var i = 0; i < parallaxQuantity; i++) 
				{
					var currentElement = parallaxElement.eq(i),
					windowTop = $(window).scrollTop(),
					elementTop = currentElement.offset().top,
					elementHeight = currentElement.height(),
					viewPortHeight = window.innerHeight * 0.5 - elementHeight * 0.5,
					scrolled = windowTop - elementTop + viewPortHeight;
					currentElement.css(
					{
						transform: "translate3d(0," + scrolled * -0.3 + "px, 0)"
					});
				}
			});

			var parallaxElementHorizontal = $(".parallax_horizontal_scroll"),
			parallaxHorizontalQuantity = parallaxElementHorizontal.length;

			window.requestAnimationFrame(function() 
			{
				for (var i = 0; i < parallaxHorizontalQuantity; i++) 
				{
					var currentElement = parallaxElementHorizontal.eq(i),
					windowTop = $(window).scrollTop(),
					elementTop = currentElement.offset().top,
					elementHeight = currentElement.height(),
					viewPortHeight = window.innerHeight * 0.5 - elementHeight * 0.5,
					scrolled = windowTop - elementTop + viewPortHeight;
					scrolledDestination = scrolled * 5;

					if (scrolledDestination < 0) scrolledDestination = 0;
					if (scrolledDestination > 800) scrolledDestination = jQuery('.wrap').width() - currentElement.width() + 100;

					currentElement.css(
					{
						transform: "translate3d(" + scrolledDestination + "px, 0, 0)"
					});
				}
			});
		});

		$('.partner-menu').on('click', function(e)
		{
			jQuery('html, body').animate({ scrollTop: $('#partner-anchor').offset().top - 150}, 750);
			e.preventDefault();
			return false;
		});

		$('.contact-menu').on('click', function(e)
		{
			jQuery('html, body').animate({ scrollTop: $('#contact-anchor').offset().top }, 750);
			e.preventDefault();
			return false;
		});

		$('.phones-modal').on('click', function()
		{
			$("html, body").animate({ scrollTop: 0 }, "fast");
			$("html,body").css("overflow-y", "hidden"); 
			$('#phones-modal-container').fadeIn('fast');
			$('#phones-container').fadeIn('fast');
			$('#phones-modal').fadeIn('fast');
		});

		$('.btn-close').on('click', function()
		{
			$("html,body").css("overflow-y", "auto");
			$('#phones-modal-container').fadeOut('fast');
			$('#phones-container').fadeOut('fast');
			$('#phones-modal').fadeOut('fast');
		});

		$('#phones-modal-container').on('click', function()
		{
			$("html,body").css("overflow-y", "auto");
			$('#phones-modal-container').fadeOut('fast');
			$('#phones-container').fadeOut('fast');
			$('#phones-modal').fadeOut('fast');
		});

		$(window).on('scroll',function()
		{
			if ($(window).scrollTop() > 50)
			{
				jQuery('#header').addClass("scrolled");
			}
	       	else
	       	{
	       		jQuery('#header').removeClass("scrolled");		
	       	}
		});

	});
})(jQuery);

function sendcontact_callback()
{
	jQuery('#contact-form-message').text('Sua mensagem foi enviada com sucesso! Entraremos em contato!');
	jQuery('#contact-form-message').show();
	jQuery('#contact-form input[type="submit"]').removeAttr('disabled');

	setTimeout(function(){ jQuery('#case-form-message').hide(); }, 5000);

	contactEmail.value = "";
	contactName.value = "";
	contactPhone.value = "";
	contactPhoneDDD.value = "";
	contactSchool.value = "";
	contactCity.value = "";
	contactState.value = "";
	contactRole.value = "";
	contactSegment.value = "";
	contactEducationSystem.value = "";
	contactDoubts.value = "";
}

function ajaxified_contact(args, callBack) 
{
	console.log('JS Ajax Called: Send contact form: '+args);

	args.append('action', 'ajaxified_sendcontactform');
	args.append('nouncecheck', ajaxScript.nouncecheck);
	
	jQuery.ajax(
	{
		type: 'POST',
		url: ajaxScript.ajaxurl,
		contentType: false,
		processData: false,
		data: args,
		success: function(data) 
		{
			window[callBack](data);
		}
	});
}