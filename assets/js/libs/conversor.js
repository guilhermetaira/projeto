function Ingrediente(e, n, i) {
    this.nome = e,
    this.liquido = n,
    this.conversao = i
}
function Medida(e, n, i, d, a) {
    this.tipo = e,
    this.nome = n,
    this.abreviacao = i,
    this.conversao = d,
    this.liquido = a
}
function criarOpcao(e, n) {
    var i = document.createElement("option");
    return i.text = e,
    i.value = n,
    i
}
function exibirIngredientes(e) {
    var n, i;
    for (n = document.createElement("select"),
    n.onchange = selecionarIngrediente,
    n.options[0] = criarOpcao("Escolha o ingrediente", -1),
    n.selectedIndex = 0,
    i = 0; i < ingredientes.length; i++)
        n.options[i + 1] = criarOpcao(ingredientes[i].nome, i);
    return document.getElementById(e).appendChild(n),
    n
}
function exibirMedidas(e) {
    var n, i;
    if (n = document.createElement("select"),
    n.onchange = selecionarMedida,
    0 > ingredienteSelecionado)
        n.options[0] = criarOpcao("- Selecione um ingrediente -", -1);
    else
        for (n.options[0] = criarOpcao("- Selecione -", -1),
        i = 0; i < medidas.length; i++)
            (!medidas[i].liquido || ingredientes[ingredienteSelecionado].liquido) && (n.options[i + 1] = criarOpcao(medidas[i].nome, i));
    for (n.selectedIndex = 0,
    elementoDestino = document.getElementById(e); elementoDestino.hasChildNodes(); )
        elementoDestino.removeChild(elementoDestino.firstChild);
    return elementoDestino.appendChild(n),
    n
}
function selecionarIngrediente() {
    ingredienteSelecionado = selectIngredientes.options[selectIngredientes.selectedIndex].value,
    selectMedidasEntrada = exibirMedidas("entradaMedidas"),
    selectMedidasSaida = exibirMedidas("saidaMedidas"),
    selecionarMedida()
}
function selecionarMedida() {
    medidaEntrada = selectMedidasEntrada.options[selectMedidasEntrada.selectedIndex].value,
    medidaSaida = selectMedidasSaida.options[selectMedidasSaida.selectedIndex].value,
    inputSaida.value = ""
}
function validarEntrada() {
    var e, n, i, d, a = "0123456789.,/";
    for (e = inputEntrada.value,
    n = "",
    i = 0; i < e.length; i++)
        d = e.charAt(i),
        -1 == a.indexOf(d) && (d = ""),
        n += d;
    for (d = n.charAt(0); a.indexOf(d) > 9; d = n.charAt(0))
        n = n.substr(1, n.length - 1);
    for (d = n.charAt(n.length - 1); a.indexOf(d) > 9; d = n.charAt(n.length - 1))
        n = n.substr(0, n.length - 1);
    for (e = n,
    n = "",
    i = 0; i < e.length; i++)
        d = e.charAt(i),
        -1 == a.indexOf(d) && (d = ""),
        a.indexOf(d) > 9 && (a = "0123456789"),
        n += d;
    n.length > 0 && (n = n.replace(".", ",")),
    inputEntrada.value = n
}
function calcularResultado() {
    var entrada = eval(inputEntrada.value.replace(",", "."))
      , saida = -1;
    ingredienteSelecionado > -1 && isFinite(entrada) && medidaEntrada > -1 && medidaSaida > -1 && inputEntrada.value.length > 0 && (saida = medidas[medidaEntrada].tipo == PESO ? entrada * medidas[medidaEntrada].conversao : entrada * ingredientes[ingredienteSelecionado].conversao[medidas[medidaEntrada].conversao],
    saida /= medidas[medidaSaida].tipo == PESO ? medidas[medidaSaida].conversao : ingredientes[ingredienteSelecionado].conversao[medidas[medidaSaida].conversao]),
    saida > -1 ? inputSaida.value = filtrarFracao(saida) : inputSaida.value = ""
}
function filtrarFracao(e) {
    var n, i, d, a, t = ["0", "1/4", "1/3", "1/2", "2/3", "3/4", "1"], r = [0, .25, 1 / 3, .5, 2 / 3, .75, 1];
    for (inteiro = Math.floor(e),
    fracao = e - inteiro,
    d = 10,
    i = 0; i < t.length; i++)
        n = Math.abs(fracao - r[i]),
        d > n && (d = n,
        a = i);
    return 1 == r[a] && (inteiro++,
    a = 0),
    inteiro > 0 ? a > 0 ? parseInt(inteiro) + " e " + t[a] : parseInt(inteiro) : a > 0 ? t[a] : "0"
}
var PESO = 1, VOLUME = 0, ingredientes = new Array, medidas = new Array, selectIngredientes, selectMedidasEntrada, selectMedidasSaida, inputEntrada, inputSaida, ingredienteSelecionado, medidaEntrada, medidaSaida;
ingredientes.push(new Ingrediente("Açúcar",!1,[180, 10, 5, 150])),
ingredientes.push(new Ingrediente("Água",!0,[240, 15, 2.5, 200])),
ingredientes.push(new Ingrediente("Amendoim cru",!1,[160, 10, 5, 120])),
ingredientes.push(new Ingrediente("Amido de milho",!1,[120, 5, 2.5, 100])),
ingredientes.push(new Ingrediente("Arroz arborio cru",!1,[210, 10, 5, 150])),
ingredientes.push(new Ingrediente("Arroz cru",!1,[175, 15, 5, 155])),
ingredientes.push(new Ingrediente("Aveia em flocos finos",!1,[115, 5, 2.5, 85])),
ingredientes.push(new Ingrediente("Café pronto",!0,[240, 15, 5, 200])),
ingredientes.push(new Ingrediente("Cebola picadinha",!1,[110, 10, 5, 90])),
ingredientes.push(new Ingrediente("Chocolate em pó / Cacau / Achocolatado",!1,[100, 10, 5, 80])),
ingredientes.push(new Ingrediente("Creme de leite de caixinha",!0,[240, 15, 5, 200])),
ingredientes.push(new Ingrediente("Creme de leite fresco",!0,[240, 15, 5, 200])),
ingredientes.push(new Ingrediente("Farinha de rosca",!1,[100, 5, 2.5, 70])),
ingredientes.push(new Ingrediente("Farinha de trigo",!1,[140, 5, 2.5, 120])),
ingredientes.push(new Ingrediente("Feijão cru",!1,[200, 10, 5, 160])),
ingredientes.push(new Ingrediente("Grão de bico cozido",!1,[170, 20, 10, 130])),
ingredientes.push(new Ingrediente("Grão de bico cru",!1,[195, 15, 5, 140])),
ingredientes.push(new Ingrediente("Leite",!0,[240, 15, 5, 200])),
ingredientes.push(new Ingrediente("Leite condensado",!0,[240, 15, 5, 200])),
ingredientes.push(new Ingrediente("Leite de coco",!0,[240, 15, 10, 200])),
ingredientes.push(new Ingrediente("Milho cru",!1,[200, 20, 10, 165])),
ingredientes.push(new Ingrediente("Passas",!1,[140, 15, 5, 100])),
ingredientes.push(new Ingrediente("Polvilho doce e azedo",!1,[155, 15, 2.5, 100])),
ingredientes.push(new Ingrediente("Sal grosso",!1,[300, 20, 5, 210])),
medidas.push(new Medida(PESO,"Quilograma","Kg",1e3,!1)),
medidas.push(new Medida(PESO,"Gramas","g",1,!1)),
medidas.push(new Medida(VOLUME,"Xícara (Chá)","",0,!1)),
medidas.push(new Medida(VOLUME,"Colher (Sopa)","",1,!1)),
medidas.push(new Medida(VOLUME,"Colher (Chá)","",2,!1)),
medidas.push(new Medida(VOLUME,"Copo (Americano)","",3,!1)),
inputEntrada = document.getElementById("quantidade-entrada"),
inputSaida = document.getElementById("quantidade-saida"),
inputEntrada.value = "",
inputEntrada.onchange = validarEntrada,
selectIngredientes = exibirIngredientes("lista-ingredientes"),
selecionarIngrediente(),
document.getElementById("converteMedida").onclick = calcularResultado;
