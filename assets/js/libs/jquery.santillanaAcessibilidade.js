// JavaScript jQuery Plugin santillanaAcessibilidade
(function($) {

	//Definicao do nome do plugin e chamada com opcoes
	jQuery.fn.santillanaAcessibilidade = function(options) {

		//Seta os valores default
		var defaults = {
			nomeCookieFiltro: "BarraAcessibilidadeFiltro",
			nomeCookieZoom: "BarraAcessibilidadeZoom",
			caminhoCss: "css/",
			zoomEscala: true,
			zoomMaximo: 8,
			zoomPorcentagem: 25,
			elementosMudancaFonte: "h1, h2, h3, h4, h5, h6, p, a, span, label, input, textarea, li, pre",
			excluirElementos: ['.nofilter, img'],
			excluirElementosAP: ['img'],
			codOrigem: '4',
		}

		var options = $.extend(defaults, options);

		//Inicializacao do plugin
		return this.each(function() {

			//
			//	Objeto principal
			//
			var obj = jQuery(this);
			
			//
			//	Remove os botoes da correspondentes da barra caso a pagina nao tenha conteudo, menu, busca ou rodape
			//
			if($('#conteudo').length <= 0){
				$('body .barraAcessivel ul:first-child li[role="menuitem"]:first-child').hide();
			}
			if($('#menuPrincipal').length <= 0){
				$('body .barraAcessivel ul:first-child li[role="menuitem"]:nth-child(2)').hide();
			}
			if($('#txtBusca').length <= 0){
				$('body .barraAcessivel ul:first-child li[role="menuitem"]:nth-child(3)').hide();
			}
			if($('#rodape').length <= 0){
				$('body .barraAcessivel ul:first-child li[role="menuitem"]:nth-child(4)').hide();
			}

			//
			//	Verificação se é firefox
			//
			var isFirefox = false;
			if ( navigator.userAgent.match(/Mozzila/) || navigator.userAgent.match(/Firefox/) ){
				isFirefox = true;
			}

			//
			//	Verificação se é Internet Explorer
			//
			var isIE = false;
			if (navigator.appName == 'Microsoft Internet Explorer' || !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv:11/)) || (typeof $.browser !== "undefined" && $.browser.msie == 1)) {
				isIE = true;
			}

			//
			//	Opções de exclusao serão incluidos com a classe noFilter
			//
			options.excluirElementos.forEach(function(item, index) {
				$(item).addClass('noFilter');
			})

			options.excluirElementosAP.forEach(function(item, index) {
				$(item).addClass('noFilterAP');
			})

			var menuContraste = obj.find("ul.pull-right li.contraste");
			var fecharMenuContraste = obj.find("ul.pull-right li.closeContrast");
			var barra = menuContraste.find("div.dautoniveis");
			var screenWidth = $(window).width();
			var zoomInBtn = obj.find(".zoomIn");
			var zoomOutBtn = obj.find(".zoomOut");

			if ( !options.tamanhoConteudo ){
				options.tamanhoConteudo = 960;
			}

			var containerX = obj.find(".container").width();
			var scale;

			if ( isFirefox ){
				$(window).resize(function() {
					containerX = obj.find(".container").width();
					screenWidth = $(window).width();
					setCookie(options.nomeCookieZoom, 1);
					zoomOut();
				});
			}

			//
			//	Verificar o cookie setado para aplicar no body
			//
			if (document.cookie.indexOf(options.nomeCookieFiltro) > 0) {
				if (getCookie(options.nomeCookieFiltro) != "normal") {
					var valueCookie = getCookie(options.nomeCookieFiltro);
					changeFilter(valueCookie);
				}
			}

			//
			//	Verificação de cookies
			//
			if (document.cookie.indexOf(options.nomeCookieZoom) < 0) {
				if (!options.zoomEscala) {
					setCookie(options.nomeCookieZoom, 0);
				} else {
					setCookie(options.nomeCookieZoom, 1);
				}
			}
			if (document.cookie.indexOf(options.nomeCookieFiltro) < 0) {
				setCookie(options.nomeCookieFiltro, 'normal');
			}

			// Cria cookie de zoom
			if (options.zoomEscala) {
				var scale = parseFloat(getCookie(options.nomeCookieZoom));
				if ( scale < 1 ) {
					setCookie(options.nomeCookieZoom, 1);
					scale = 1;
				}
			} else {
				var scale = parseFloat(getCookie(options.nomeCookieZoom));
			}

			//
			//	Clique nos níveis de contraste
			//
			barra.find("a").click(function(event) {
				var nivel = $(this).attr("data-nivel");

				//Cria o cookie com o filtro selecionado
				setCookie(options.nomeCookieFiltro, nivel, 0);

				//aplica o filtro chamando o metodo de aplicar filtro
				changeFilter(nivel);

				if ($(window).width() <= 992) {
					barra.removeClass('opened').addClass('closed');
				}
				return false;
			});

			//
			//	Verificação contraste para IE ( Link contraste aplica e remove o contraste amarelo no preto )
			//
			if ( isIE ) {

				var btnContraste = barra.parent().find("a");
				barra.remove();

				if ( getCookie(options.nomeCookieFiltro) == "amarelopreto" ){
					btnContraste.attr("data-nivel","normal");
				}else{
					btnContraste.attr("data-nivel","amarelopreto");
				}

				// Botao Contraste aplicando filtro
				btnContraste.click(function(event) {

					var btn = $(this);
					var nivel = $(this).attr("data-nivel");
					setCookie(options.nomeCookieFiltro, nivel, 0);
					changeFilter(nivel);
					if( nivel == "amarelopreto"){
						btn.attr("data-nivel","normal");
					}else{
						btn.attr("data-nivel","amarelopreto");
					}
					return false;

				});

			}

			//
			//	Função aplicar filtro
			//
			function changeFilter(nivel) {
				switch (nivel) {
					// Caso default
					case 'normal':
						resetFilter();
						break;
						// Caso for alto contraste
					case 'altocontraste':
						resetFilter();
						$("head").append('<link id="santAcess-altocontraste" rel="stylesheet" type="text/css" href="'+ options.caminhoCss + 'sa.altocontraste.css" />');
						break;
						// Caso for monocromático
					case 'monocromatico':
						resetFilter();
						$("head").append('<link id="santAcess-monocromatico" rel="stylesheet" type="text/css" href="'+ options.caminhoCss + 'sa.monocromatico.css" />');
						break;
						// Caso for escala de cinza
					case 'cinzainvertida':
						resetFilter();
						$("head").append('<link id="santAcess-cinzainvertida" rel="stylesheet" type="text/css" href="'+ options.caminhoCss + 'sa.cinzainvertida.css" />');
						break;

						// Caso de cores invertidas
					case 'corinvertida':
						resetFilter();
						$("head").append('<link id="santAcess-corinvertida" rel="stylesheet" type="text/css" href="'+ options.caminhoCss + 'sa.corinvertida.css" />');
						break;

						// Caso de amarelo no preto
					case 'amarelopreto':
						resetFilter();
						$("head").append('<link id="santAcess-amarelopreto" rel="stylesheet" type="text/css" href="'+ options.caminhoCss + 'sa.amarelopreto.css" />');
						break;
				}
			}

			//
			//	Funcao resetar filtros
			//
			function resetFilter() {
				$('link[id^="santAcess-"]').remove();
			}

			//
			//	Adiciona a fonte base ao projeto
			//
			if (!options.zoomEscala) {
				$(options.elementosMudancaFonte).each(function() {
					var obj = $(this);
					var itemFontSize = obj.css("font-size");
					var itemFontSize = itemFontSize.replace("px", "").trim();

					var itemLineHeight = obj.css("line-height");
					var itemLineHeight = itemLineHeight.replace("px", "").trim();

					obj.attr("data-font", itemFontSize);
					obj.attr("data-lheight", itemLineHeight);
				});
			}

			//
			//	Funcao para mudar a fonte
			//
			function changeFont(zoom, porcentagem) {
				if (!porcentagem) {
					porcentagem = options.zoomPorcentagem;
				}
				$(options.elementosMudancaFonte).each(function() {
					var obj = $(this);
					var itemFontSize = obj.attr("data-font");
					var itemLineHeight = obj.attr("data-lheight");

					if (zoom == 0) {
						var novoTamanho = itemFontSize;
						var novoTamanhoLh = itemLineHeight;
					} else {
						var calculo = Math.floor(parseFloat(porcentagem) * zoom) / 100;
						var multiplicador = parseFloat(1.0) + calculo;
						var novoTamanho = parseFloat(itemFontSize) * multiplicador;
						var novoTamanhoLh = parseFloat(itemLineHeight) * multiplicador;
					}

					obj.css({
						"font-size": novoTamanho + "px",
						"line-height": novoTamanhoLh + "px"
					});
				});
			}

			//
			//	Função verifica tela
			//
			function verify(type) {
				if (type == 'in') {
					var newSize = containerX * (scale + 0.2);
					if (newSize < screenWidth) {
						return true;
					} else {
						return false;
					}
				}
				if (type == 'out') {
					var newSize = containerX - (containerX / (scale - 0.2));
					if (newSize > 0) {
						return true;
					} else {
						return false;
					}
				}
			}

			var currFFZoom = parseFloat(getCookie(options.nomeCookieZoom));
			var currIEZoom = ( parseFloat(currFFZoom) ) * 100;
			var valid = false;

			function plus(){
				var step = 0.2;
				currFFZoom += step;
				$('body').css('MozTransform','scale(' + currFFZoom + ')');
				var stepie = 20;
				currIEZoom += stepie;
				$('body').css('zoom', ' ' + currIEZoom + '%');
				setCookie(options.nomeCookieZoom, currFFZoom);
				centralizarZoom();
			};
			function minus(){
				if ( currFFZoom > 1) {
					var step = 0.2;
					currFFZoom -= step;
					$('body').css('MozTransform','scale(' + currFFZoom + ')');
					var stepie = 20;
					currIEZoom -= stepie;
					$('body').css('zoom', ' ' + currIEZoom + '%');
					setCookie(options.nomeCookieZoom, currFFZoom);
					centralizarZoom();
				}
			};

			//
			//	Centralizar no zoom
			//
			function centralizarZoom(){
				$('body').width("100%");
				if ( $(".container").outerWidth() > $(".container").outerWidth(true) ){
					$('body').width(window.innerWidth);
				}else{
					$('body').width("100%");
				}
				var ld = $(document).width() - $('body').width();
				var nv = ld / 2;
				window.scrollTo( nv, $('body').offset().top);
			}

			//
			//	Função zoomIn em escala
			//
			function zoomIn() {
				if (verify('in') == true) {
					scale += 0.2;
					$('body').css({
						'transform': 'scale(' + scale + ',' + scale + ')',
						'transformOrigin': '50% 0',
						'overflow-x': 'hidden'
					});
					setCookie(options.nomeCookieZoom, scale);
				}
			}

			//
			//	Função zoomOut em escala
			//
			function zoomOut() {
				if (verify('out') == true) {
					scale -= 0.2;
					$('body').css({
						'transform': 'scale(' + scale + ',' + scale + ')',
						'transformOrigin': '50% 0'
					});
				} else {
					scale = 1;
					$('body').css({
						'transform': 'scale(' + scale + ',' + scale + ')',
						'transformOrigin': '50% 0',
						'overflow-x': 'auto'
					});
				}
				setCookie(options.nomeCookieZoom, scale);
			}

			//
			//	Clique para zoom
			//
			zoomInBtn.click(function () {
				if (!options.zoomEscala) {
					var zoom = parseInt(getCookie(options.nomeCookieZoom)) + 1;
					if (zoom < (options.zoomMaximo + 1)) {
						changeFont(zoom);
					}
					if (zoom <= options.zoomMaximo) {
						setCookie(options.nomeCookieZoom, zoom);
					}
				} else {
					if ( isFirefox ){
						zoomIn();
					}else{
						plus();
					}
				}
				return false;
			});

			//
			//	Clique para zoom
			//
			zoomOutBtn.click(function() {
				if (!options.zoomEscala) {
					var zoom = parseInt(getCookie(options.nomeCookieZoom)) - 1;
					if (zoom >= 0) {
						setCookie(options.nomeCookieZoom, zoom);
						changeFont(zoom);
					}
				} else {
					if ( isFirefox ){
						if (scale > 1) {
							zoomOut();
						}
					}else{
						minus();
					}
				}
				return false;
			});

			var cookieZoom = getCookie(options.nomeCookieZoom);
			if (!options.zoomEscala) {
				changeFont(cookieZoom);
			} else {
				if ( isFirefox ){
					if (cookieZoom == 1) {
						$('body').css({
							'transform': 'scale(' + cookieZoom + ',' + cookieZoom + ')',
							'transformOrigin': '50% 0',
							'overflow-x': 'auto'
						});
					} else {
						$('body').css({
							'transform': 'scale(' + cookieZoom + ',' + cookieZoom + ')',
							'transformOrigin': '50% 0',
							'overflow-x': 'hidden'
						});
					}
				}else{
					var cFF = cookieZoom;
					$('body').css('MozTransform','scale(' + cFF + ')');
					var cIE = ( parseFloat(cookieZoom) ) * 100;
					$('body').css('zoom', ' ' + ( 0.100 + cIE ) - 0.1 + '%');
					centralizarZoom();
				}
			}

			//
			//	Verificação para hover e/ou clique para abrir menu de itens da barra
			//
			if (screenWidth > 992) {
				if (!menuContraste.find('a').hasClass('contrasteOff')) {
					menuContraste.find('a').hover(function() {
						menuContraste.find('div.dautoniveis').removeClass('closed').addClass('opened');
					}, function() {
						menuContraste.find('div.dautoniveis').removeClass('opened').addClass('closed');
					}).click(function() {
						if (barra.hasClass('opened')) {
							barra.removeClass('opened').addClass('closed');
						} else {
							barra.removeClass('closed').addClass('opened');
						}
					});
				}
				fecharMenuContraste.find('a').focusin(function() {
					barra.removeClass('opened').addClass('closed');
				});
			} else {
				menuContraste.find(' > a').on('click', function() {
					if (barra.hasClass('closed')) {
						barra.removeClass('closed').addClass('opened');
					} else {
						barra.removeClass('opened').addClass('closed');
					}
				});
			}
			
			
			//
			//	Monta link de Livro acessível de acordo com o código de origem
			//
			var urlLivroAcessivel = $('.barraAcessivel ul li a.livroAcessivel').attr('href');
			if(urlLivroAcessivel != "#" && urlLivroAcessivel != "javascript:void(0);"){
				$('.barraAcessivel ul li a.livroAcessivel').attr('href', urlLivroAcessivel + options.codOrigem);
			}

			//
			//	Funcao Set Cookie
			//
			function setCookie(nome, valor, dias) {
				if (!dias) {
					dias = 0;
				}
				if (dias) {
					var date = new Date();
					date.setTime(date.getTime() + (dias * 24 * 60 * 60 * 1000));
					var expires = "; expires=" + date.toGMTString();
				} else var expires = "";
				document.cookie = nome + "=" + valor + expires + "; path=/";
			}

			//
			//	Funcao Get Cookie
			//
			function getCookie(cname) {
				var name = cname + "=";
				var decodedCookie = decodeURIComponent(document.cookie);
				var ca = decodedCookie.split(';');
				for (var i = 0; i < ca.length; i++) {
					var c = ca[i];
					while (c.charAt(0) == ' ') {
						c = c.substring(1);
					}
					if (c.indexOf(name) == 0) {
						return c.substring(name.length, c.length);
					}
				}
				return "";
			}

		}); //Fim do processo

	}
})(jQuery);