/*
*
*	Google Analytics Plugin Acessibilidade Santillana
*	21/07/2017
*
 */
(function(){

	// Simplificar envio
	jQuery.fn.simplifyAnalyticsSantillana = function(options){

		//Seta os valores default
		var defaults = {
			categoria:"Categoria",
			atributo: "title",
			evento: "Clique",
			valor:"",
			prefix:""
		}

		var options = $.extend(defaults, options);

		//Inicializacao do plugin
		return this.each(function(){

			//Objeto
			var obj = jQuery(this);

			obj.click(function() {

				var xCategory = options.categoria;
				if ( options.evento == "Clique"){
					var xEvent = options.evento;
				}else{
					var xEvent = obj.attr(options.evento);
				}

				var xValue = obj.attr(options.atributo);

				if ( options.valor != ""){
					var xValue = $(options.valor).html();
				}

				var xPrefix = "";
				if ( options.prefix != ""){
					xPrefix = options.prefix + " - ";
				}

				// Verifica se é a versão universal ( mais utilizada e mais atual do analytics )
				if ( typeof GoogleAnalyticsObject != "undefined" ) {
					if ( typeof ga != "undefined" ){
						ga("send", "event" , xCategory, xEvent, xPrefix + xValue);
					}
				}else if( typeof _gaq !="undefined" ){ // Verifica se é a versão clássica
					_gaq.push(['_trackEvent', xCategory, xEvent, xPrefix + xValue]);
				}

			});

		}); //Fim do processo

	}

})(jQuery);

$(document).ready(function($) {

	// Aplicação do plugin para todos os itens de lista da barra de acessiblidade.
	$('.barraAcessivel ul li a').simplifyAnalyticsSantillana({"categoria":"Acessibilidade"});

});